﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NSoup;
using NSoup.Nodes;
using NSoup.Parse;
using System.IO;

namespace GrimDawnBuildScraper
{
    class SkillInfo
    {
        public string Title { get; set; }
        public List<string> Bits { get; set; }
    }

    class RaceBuild
    {
        public string Race { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        public string Author { get; set; }
        public List<SkillInfo> SkillInfo { get; set; }
        public List<string> Tags { get; set; }
    }


    class Program
    {
        static void Main(string[] args)
        {
            //This is the compendium thread
            var site = "http://www.grimdawn.com/forums/showthread.php?t=48165/";
            var timeout = TimeSpan.FromSeconds(10);

            var stopwatch = Stopwatch.StartNew();

            var builds = ScrapeBuilds(site, timeout);

            stopwatch.Stop();

            using (StreamWriter file = File.CreateText(@"D:\Media\Desktop\temp shit\GDBuilds.json"))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Formatting = Formatting.Indented;
                //serialize object directly into file stream
                serializer.Serialize(file, new { Builds = builds });
            }

            //output the JSON object
            //Console.WriteLine(JsonConvert.SerializeObject(new {Builds = builds}, Formatting.Indented));

            Console.WriteLine($"Took {stopwatch.Elapsed.Seconds:F} seconds");
            Console.ReadLine();
        }

        private static List<RaceBuild> ScrapeBuilds(string site, TimeSpan timeout)
        {
            Document dom = NSoupClient.Parse(new Uri(site), (int) timeout.TotalMilliseconds);
            List<RaceBuild> builds = new List<RaceBuild>();

            var posts = dom.GetElementById("posts");

            foreach (Element post in posts.Children.Where(c => c.Tag.Name == "div"))
            {
                Element curClassTitle = new Element(Tag.ValueOf("empty"),"");
                Element[] headers = post.GetElementsByTag("blockquote").ToArray();
                int curHeaderIndex = 0;
                while(curHeaderIndex!=headers.Length)
                {
                    Element curHeader = headers[curHeaderIndex];

                    // Firstly check to see if the current blockquote is a classtitle
                    if (curHeader.Parent.Tag.Name == "font" &&
                        curHeader.Parent.Attributes["Size"] == "+1")
                    {
                        curClassTitle = headers[curHeaderIndex];
                    }

                    if (headers[curHeaderIndex] != null)
                    {
                        if (curClassTitle.Tag.Name != "empty")
                            builds.Add(BuildFromElement(curClassTitle, headers[curHeaderIndex]));
                    }

                    curHeaderIndex++;
                }
                //foreach (Element headers in post.GetElementsByTag("blockquote")
                //.Where(q => q.Parent.Tag.Name == "font" && q.Parent.Attributes["Size"] == "+1")
                //.Where(q => q.Parent.NextElementSibling != null &&
                //    q.Parent.NextElementSibling.Tag.Name == "blockquote"))
                //    {
                //        foreach (Element buildInfo in headers.Parent
                //            .SiblingElements // this is all siblings, not just the ones after this element
                //            .Where(el => el.Tag.Name == "blockquote"))
                //        {
                //            Console.WriteLine(buildInfo.ToString());
                //            Console.WriteLine("-------------------------------------------------------");
                //            builds.Add(BuildFromElement(headers, buildInfo));
                //        }
                //    }
            }

            /*builds.Add(
                new RaceBuild
                {
                    Race = "Demolitionist",
                    Title = "[Ranged, Caster] Technomancer (Dandy)",
                    Tags = new List<string> {"Ultimate difficulty", "Video", "G1-5"},
                    Link = "http://www.grimdawn.com/forums/showthread.php?t=41355",
                    SkillInfo = new List<SkillInfo>
                    {
                        new SkillInfo
                        {
                            Title = "Damage",
                            Bits = new List<string> {"Fire", "Lightning"}
                        }
                        // and more here....
                    }
                });*/

            return builds.Where(b => b != null).ToList();
        }

        private static string RemoveBracketsFromString(string inputString)
        {
            return inputString.Replace("(", "")
                .Replace(")", "")
                .Replace("[", "")
                .Replace("]", "");
        }

        private static RaceBuild BuildFromElement(Element headers, Element buildInfo)
        {
            //basic pattern for characters in brackets like (this) and [this]
            string tagMatcher = @"[\(\[][\d\w\s]{1,}[\)\]]";

            Dictionary<string, string> TagNames = new Dictionary<string, string>(
                StringComparer.OrdinalIgnoreCase)
                {
                    { "u" , "Ultimate" },
                    { "hc", "HardCore" },
                    { "c",  "Crucible" },
                    { "c+", "Crucible - Gladiator" },
                    { "d",  "Downloadable Save File" },
                    { "vid", "Video Available" },
                    { "g1" , "Gear Rating 1" }, //maybe there's a nicer way to do this, I don't know :(
                    { "g2" , "Gear Rating 2" },
                    { "g3" , "Gear Rating 3" },
                    { "g4" , "Gear Rating 4" },
                    { "g5" , "Gear Rating 5" }
                };

            RaceBuild returnRaceBuild = new RaceBuild();
            returnRaceBuild.Race = headers.Text();

            //"This is easy!" he said, "It'll be fun!" he said
            string linkCheck = buildInfo.Select("a[href]").Attr("href");
            returnRaceBuild.Link = ((linkCheck.StartsWith("http://www.grimdawn.com/forums/") ||
                linkCheck.StartsWith("http://grimdawn.com/forums/") ? "" : "http://www.grimdawn.com/forums/")
                + linkCheck) ?? "";
            //Link grabbing....DONE!

            string potentialTitle = buildInfo.Select("a[href]").Html();
            if (potentialTitle != "")
            {
                returnRaceBuild.Title = potentialTitle;
            }
            else
                return null;
            string authorLine = buildInfo.Select("ul>li>font").Html();
            string authorMatch = Regex.Match(authorLine, tagMatcher, RegexOptions.RightToLeft).Value;

            // So before the first break, we pass the title of the build, as well as the tags, title and author
            string buildInfoString = buildInfo.ToString();

            returnRaceBuild.Tags = new List<string>();

            // All about the tags
            foreach (Match match in Regex.Matches(buildInfoString, tagMatcher))
            {
                string fixedMatch = RemoveBracketsFromString(match.Value);
                if (TagNames.ContainsKey(fixedMatch))
                    returnRaceBuild.Tags.Add(TagNames[fixedMatch]);
                else
                {
                    // hopefully the only thing triggering the regex match that doesn't match a
                    // pre existing tag is the author
                    if (match.NextMatch().Success)
                        returnRaceBuild.Tags.Add(fixedMatch);
                }
            }

            //Nick I am so god damned sorry
            int SkillInfoCategory = 0;
            returnRaceBuild.SkillInfo = new List<SkillInfo>();
            SkillInfo returnSkillInfo;

            string[] skillInfoCatDesc = {
                    "Damage",
                    "Active Skills",
                    "Passive Skills",
                    "Default Attack Skills (WPS)"
            };

            // Time to get skilled up
            foreach (Element line in buildInfo.Select("blockquote>ul>li>ul>li")) {

                returnSkillInfo = new SkillInfo();
                string txt = line.Text();
                int startSub = txt.IndexOf(":")+1; // 3hacks5me
                int endSub = txt.Length;

                if (startSub > 0 && endSub > 0)
                {
                    txt = txt.Substring(startSub, endSub - (startSub));

                    returnSkillInfo.Title = skillInfoCatDesc[SkillInfoCategory];

                    returnSkillInfo.Bits = txt.Split(',').ToList<string>();
                    returnRaceBuild.SkillInfo.Add(returnSkillInfo);
                }
                // Next category
                SkillInfoCategory = (SkillInfoCategory + 1) % 4;
            }

            
            // Author is the last thing tagged
            returnRaceBuild.Author = RemoveBracketsFromString(authorMatch);

            return returnRaceBuild;
        }
    }
}
